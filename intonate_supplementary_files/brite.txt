br:br08901	KEGG pathway maps
br:br08902	BRITE hierarchy files
br:br08904	BRITE table files
br:br08907	KEGG networks
br:ko00001	KEGG Orthology (KO)
br:ko00002	KEGG modules
br:ko00003	KEGG reaction modules
br:ko01000	Enzymes
br:ko01001	Protein kinases
br:ko01009	Protein phosphatases and associated proteins
br:ko01002	Peptidases and inhibitors
br:ko01003	Glycosyltransferases
br:ko01005	Lipopolysaccharide biosynthesis proteins
br:ko01011	Peptidoglycan biosynthesis and degradation proteins
br:ko01004	Lipid biosynthesis proteins
br:ko01008	Polyketide biosynthesis proteins
br:ko01006	Prenyltransferases
br:ko01007	Amino acid related enzymes
br:ko00199	Cytochrome P450
br:ko00194	Photosynthesis proteins
br:ko03000	Transcription factors
br:ko03021	Transcription machinery
br:ko03019	Messenger RNA biogenesis
br:ko03041	Spliceosome
br:ko03011	Ribosome
br:ko03009	Ribosome biogenesis
br:ko03016	Transfer RNA biogenesis
br:ko03012	Translation factors
br:ko03110	Chaperones and folding catalysts
br:ko04131	Membrane trafficking
br:ko04121	Ubiquitin system
br:ko03051	Proteasome
br:ko03032	DNA replication proteins
br:ko03036	Chromosome and associated proteins
br:ko03400	DNA repair and recombination proteins
br:ko03029	Mitochondrial biogenesis
br:ko02000	Transporters
br:ko02044	Secretion system
br:ko02042	Bacterial toxins
br:ko02022	Two-component system
br:ko02035	Bacterial motility proteins
br:ko04812	Cytoskeleton proteins
br:ko04147	Exosome
br:ko02048	Prokaryotic defense system
br:ko04030	G protein-coupled receptors
br:ko04050	Cytokine receptors
br:ko04054	Pattern recognition receptors
br:ko03310	Nuclear receptors
br:ko04040	Ion channels
br:ko04031	GTP-binding proteins
br:ko04052	Cytokines and growth factors
br:ko04515	Cell adhesion molecules
br:ko04090	CD molecules
br:ko00535	Proteoglycans
br:ko00536	Glycosaminoglycan binding proteins
br:ko00537	Glycosylphosphatidylinositol (GPI)-anchored proteins
br:ko04091	Lectins
br:ko01504	Antimicrobial resistance genes
br:ko03200	Viral proteins
br:ko03100	Non-coding RNAs
br:br08001	Compounds with biological roles
br:br08002	Lipids
br:br08003	Phytochemical compounds
br:br08021	Glycosides
br:br08005	Bioactive peptides
br:br08006	Endocrine disrupting compounds
br:br08007	Pesticides
br:br08008	Carcinogens
br:br08009	Natural toxins
br:br08010	Target-based classification of compounds
br:br08201	Enzymatic reactions
br:br08202	IUBMB reaction hierarchy
br:br08204	Reaction class
br:br08203	Glycosyltransferase reactions
br:br08303	Anatomical Therapeutic Chemical (ATC) classification
br:br08302	USP drug classification
br:br08301	Therapeutic category of drugs in Japan
br:br08313	Classification of Japanese OTC drugs
br:br08312	Risk category of Japanese OTC drugs
br:br08304	Traditional Chinese Medicine in Japan
br:br08305	Crude drugs
br:br08316	Pharmaceutical additives in Japan
br:br08310	Target-based classification of drugs
br:br08311	Drugs listed in the Japanese Pharmacopoeia
br:br08321	Essential oils
br:br08322	Medicinal herbs
br:br08323	Major components of natural products
br:br08331	Animal drugs in Japan
br:br08402	Human diseases
br:br08401	Infectious diseases
br:br08403	Human diseases in ICD-11 classification
br:br08411	ICD-11 International Classification of Diseases
br:br08410	ICD-10 International Classification of Diseases
br:br08420	ICD-O-3 International Classification of Diseases for Oncology
br:br08601	KEGG organisms
br:br08610	KEGG organisms in the NCBI taxonomy
br:br08620	KEGG viruses in the NCBI taxonomy
br:br08602	Animal classification
br:br08603	Plant classification
br:br08605	Plant pathogens
br:br08607	Endosymbionts
br:br08501	Human organ systems
br:br08502	Human cells
br:br01553	Antimicrobial resistance: beta-lactamase genes
br:br01554	Antimicrobial resistance: aminoglycoside resistance genes
br:br01556	Antimicrobial resistance: tetracycline resistance genes
br:br01555	Antimicrobial resistance: macrolide resistance genes
br:br01557	Antimicrobial resistance: others
br:br01800	Sequence data for EC numbers
br:br01810	Sequence data for CYP nomenclature
br:br01610	Ribosomal proteins
br:br01611	RNA polymerases
br:br01612	DNA polymerases
br:br01613	Aminoacyl-tRNA synthetases
br:br01601	Enzymes of 2-oxocarboxylic acid metabolism
br:br01602	Dioxygenases
br:br01600	Antimicrobial resistance: KEGG signatures
br:br01620	Photosynthetic and chemosynthetic capacities
br:br08319	New drug approvals in the USA
br:br08329	New drug approvals in Europe
br:br08318	New drug approvals in Japan
br:br08328	New drug approvals in the USA, Europe and Japan
br:br08309	Drug metabolizing enzymes and transporters
br:br08341	Pharmacogenomic biomarkers
br:br08324	Prodrugs
br:br08317	Topical steroids potency
br:br08315	Rx-to-OTC switch list in the USA
br:br08314	Rx-to-OTC switch list in Japan
br:br08442	Tumor markers
br:br08441	Cancer-accociated carbohydrates
br:br08431	Carbohydrates in viral and bacterial infections
