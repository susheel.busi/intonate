#!/bin/bash -l 
# this script can be used to get the coverages for each bin selected by DASTool after running IMP3

cd /scratch/users/ldenies/mice_AMR

# Thanks to the new IMP3, the coverages for the bins are already calculated in the following files:
# "$sample"/run1/Stats/mg/mg.bins.tsv
# Example below to figure out which columns we need
WORD="bin"; head -n1 /scratch/users/ldenies/mice_AMR/15_MG_Paul_S13/run1/Stats/mg/mg.bins.tsv | tr "\t" "\n" | grep -n $WORD
# 3
WORD="Coverage"; head -n1 /scratch/users/ldenies/mice_AMR/15_MG_Paul_S13/run1/Stats/mg/mg.bins.tsv | tr "\t" "\n" | grep -n $WORD
# 32

# getting coverages for bins from each sample
mkdir coverages 
for file in `cat final_mice_amr_list`
do
  awk -F"\t" '{print $0=$1"\t"$3"\t"$32}' /scratch/users/ldenies/mice_AMR/"$file"/run1/Stats/mg/mg.bins.tsv | awk -F'\t' '$1!=""' | awk '{print $0=$2"\t"$3}' |\
    sed 1d > /scratch/users/ldenies/mice_AMR/coverages/"$file"_coverages.txt
done
