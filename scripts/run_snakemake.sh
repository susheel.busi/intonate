#!/bin/bash -l
export PYTHONNOUSERSITE=true
SLURM_ARGS="-p {cluster.partition} -N 1 -n {cluster.n} -c {cluster.ncpus} -t {cluster.time} --job-name={cluster.job-name} -o {cluster.output} -e {cluster.error}"
(date; conda activate snakemake; snakemake -np -s Snakefile --unlock; snakemake -j 6 --cluster-config cluster.json --cluster "sbatch $SLURM_ARGS" -s Snakefile -p --use-conda; date) &> smk.log
