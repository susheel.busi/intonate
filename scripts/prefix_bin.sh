#!/bin/bash -l

for i in `cat list`
do
	cd $i
	for f in * ; do mv -- "$f" "$i"_"$f" ; done
	cd ../
done
