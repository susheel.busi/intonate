#!/bin/bash -l
# this script can be run to collate the checkM output into a readable .txt format
# run this only after the checkM runs via the "taxonomy_snakefile" are completed

cd /scratch/users/ldenies/mice_AMR

for file in `cat final_mice_amr_list`
do
  cd "$file"/run1/checkm_output
  sed '1,/Bin Id/d;/INFO/,$d' "$file"_output.txt | \
  sed '/^-----/d' | \
    awk '{print $1"\t"$13"\t"$14"\t"$15}' | \
      sed $'1 i\\\nbin\tcompletion\tcontamination\tstrain_heterogeneity' > "$file"_checkm.txt 
  cd /scratch/users/ldenies/mice_AMR
done
