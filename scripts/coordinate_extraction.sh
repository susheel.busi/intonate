#!/bin/bash -l

########################################
# Linking integrons and Gene cassettes #
########################################
# Date: 2020-11-23 13:16:31
cd /scratch/users/ldenies/migfinder/mice_AMR/Integron_prediction
mkdir integron_track && cd integron_track

# saved the "complete integron" files that LdN sent via slack
# file1: Integron_AMR_Treatment_complete_IDs.csv
# files2: Integron_AMR_Treatment_all_IDs.csv
# symlinking attc and intI files
for file in ../*.attc.txt; do ln -vs "$file" .; done
for file in ../*.intI.txt; do ln -vs "$file" .; done

cat *attc.txt > merged_attc.txt
sed -i '2,${/^hit/d;}' merged_attc.txt

cat *intI.txt > merged_intI.txt
sed -i '/^#/d' merged_intI.txt
sed -i $'1 i\\\ntarget name\taccession\tqueryname\taccession\thmmfrom\thmmto\talifrom\talito\tenvfrom\tenvto\tmodlen\tstrand\tE-value\tscore\tbias\tdescription_of_target' merged_intI.txt

# getting contigs of interest
awk -F"," '{print $3}' Integron_AMR_Treatment_complete_IDs.csv | sed '1d' | sed 's/\"//g' > complete_integron_list

# getting attC and intI coordinates
for line in $(cat complete_integron_list)
do
  grep -w "$line" merged_attc.txt
done | awk -F ' ' '{print $2"\t"$3"\t"$4}' | sed $'1 i\\\ncontig\tattcstart\tattcend' > attC_coords.txt

for line in $(cat complete_integron_list)
do
  grep -w "$line" merged_intI.txt
done | awk -F ' ' '{print $3"\t"$7"\t"$8"\t"$12"\t"$14}' | sed $'1 i\\\ncontig\tintIstart\tintIend\tstrand\tscore' > intI_coords.txt

# merging files with python3 - pandas
conda activate anvio-6.2
python3
import os
import pandas as pd

attc=pd.read_csv("attC_coords.txt", header=0, sep="\t")
intI=pd.read_csv("intI_coords.txt", header=0, sep="\t")

# # changing to string
# attc['contig'] = attc['contig'].astype(str)
# intI['contig'] = intI['contig'].astype(str)

df=pd.merge(attc, intI, on='contig', how='left')

# getting highest and lowest coordinate
df["min"]=df[["attcstart", "attcend", "intIstart", "intIend"]].min(axis=1)
df["max"]=df[["attcstart", "attcend", "intIstart", "intIend"]].max(axis=1)

# removing certain columns
df_edited=df.drop(columns=["attcstart", "attcend", "intIstart", "intIend"])

# getting single min and max value for same contigs
# here: https://www.kite.com/python/answers/how-to-apply-multiple-functions-to-multiple-columns-in-a-grouped-pandas-dataframe-in-python
function_dict={"min": "min", "max": "max"}
grouped = df_edited.groupby(['contig','strand', 'score'], as_index=False).aggregate(function_dict)

# writing file
df.to_csv("complete_integrons_attc_intI_coords.txt", sep='\t', index=False, header=True)
grouped.to_csv("complete_integrons_attc_intI_MinMax_coords.txt", sep='\t', index=False, header=True)

#####################
# BED file creation #
#####################
df=pd.read_csv("complete_integrons_attc_intI_MinMax_coords.txt", skiprows=1, header=None, sep='\t')

# making a new column and editing the text within, i.e. removing everything after "_contig"
df['sample']=df['contig'].str.replace("_contig.*$", "", regex=True)

# rearrange columns
df=df[['sample', 'contig', 'min', 'max', 'strand', 'score']]
df.to_csv("bed_coords_all.txt", sep='\t', index=False, header=None)

# adding column names
df.columns=["sample", "contig", "start", "end", "strand", "score"]

# saving the contig, start and end columns to file with extension ".bed"
for df_name, df_group in df.groupby('sample'):
    print(df_name, df_group.head())
    df_group.drop(['sample'], axis=1).to_csv(df_name + ".bed", sep='\t', header=None, index=False )
