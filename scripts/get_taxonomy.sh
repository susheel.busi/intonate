#!/bin/bash -l
# this script can be run to collate the checkM output into a readable .txt format
# run this only after the checkM runs via the "taxonomy_snakefile" are completed
# also below commands can be used to merge the gtdbtk and checkM outputs

cd /scratch/users/ldenies/mice_AMR

# to fix the checkM output
for file in `cat final_mice_amr_list`
do
  cd "$file"/run1/checkm_output
  sed '1,/Bin Id/d;/INFO/,$d' "$file"_output.txt | \
  sed '/^-----/d' | \
    awk '{print $1"\t"$13"\t"$14"\t"$15}' | \
      sed $'1 i\\\nbin\tcompletion\tcontamination\tstrain_heterogeneity' > "$file"_checkm.txt 
  cd /scratch/users/ldenies/mice_AMR
done

# collecting the gtdbtk information
for file in `cat final_mice_amr_list`
do
  echo $file
  cd "$file"/run1/gtdbtk_output
  awk '{print $1"\t"$2}' classify/gtdbtk.bac120.summary.tsv | sed 's/user_genome/bin/g' > "$file"_gtdbtk.txt
  cd /scratch/users/ldenies/mice_AMR
done

#merging files based on common first column
mkdir taxonomy
for file in `cat final_mice_amr_list`
do
  awk -F"\t" 'BEGIN{OFS="\t"} {if (NR==FNR) {a[$1]=$2"\t"$3"\t"$4; next} if ($1 in a) {print $1, $2, a[$1]}}' \
    "$file"/run1/checkm_output/"$file"_checkm.txt "$file"/run1/gtdbtk_output/"$file"_gtdbtk.txt \
        > taxonomy/"$file"_taxonomy_contamination.txt 
done

# keeping only those bins with completion over 60% and contamination below 10%
for file in `cat final_mice_amr_list`
do
  awk '$3>=60 && $4 <10 {print}' taxonomy/"$file"_taxonomy_contamination.txt > taxonomy/"$file"_HQ_bins.txt
done
