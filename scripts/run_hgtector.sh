#!/bin/bash -l
#SBATCH -J hgtector
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 18
#SBATCH --time=01-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=qos-bigmem

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

cd /scratch/users/ldenies/mice_AMR/hgt/hgtdb
#conda activate renv	# for the R libraries required for metaCHIP - ape, circlize and optparse
#conda activate metachip
conda activate hgtector

#ml swenv/default-env/v1.1-20180716-production
#ml lang/Python/3.6.4-intel-2018a
#export PATH=$PATH:"~/.local/bin"
#export PATH=$PATH:"~/apps/miniconda3/bin"
export PATH=$PATH:"/home/users/sbusi/apps/metachip/bin/"
#export PATH=$PATH:"/home/users/sbusi/apps/metachip/"
export PATH=$PATH:"/home/users/sbusi/apps/miniconda3/envs/anvio6/bin"	# for the hmm, prodigal tools
#export PATH=$PATH:"/work/projects/ecosystem_biology/local_tools/FastTree"

#rm prot.accession2taxid
#echo $'accession\taccession.version\ttaxid' > prot.accession2taxid
#zcat taxon.map.gz | awk -v OFS='\t' '{split($1, a, "."); print a[1], $1, $2}' >> prot.accession2taxid
# diamond makedb --threads 16 --in db.faa --taxonmap prot.accession2taxid --taxonnodes taxdump/nodes.dmp --taxonnames taxdump/names.dmp --db diamond/db
#rm prot.accession2taxid

for i in `cat list`
do 
#	hgtector search -i /scratch/users/ldenies/mice_AMR/Akkermansia/"$i".fa -o hgtector_"$i" -m diamond -d diamond/db.dmnd -t taxdump
	hgtector analyze -i hgtector_"$i"/"$i".tsv -o hgtector_analyze_"$i" -t taxdump 
done


echo "== Ending run at $(date)"
