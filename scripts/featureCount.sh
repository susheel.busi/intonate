#! /bin/bash -l
#SBATCH -J AMR
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=laura.denies@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --time=1-00:00:00


echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

export PATH="/home/users/smartinezarbas/tools/subread-1.6.4-source/bin:$PATH"
threads=12

for i in `cat list`
do

	featureCounts -p -O -t CDS -g ID -f -o ${i}.feature.counts.txt -a $i.gff -T $threads $i/mg.reads.sorted.bam

done


