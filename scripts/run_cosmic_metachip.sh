#!/bin/bash -l
#SBATCH -J metachip
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 18
#SBATCH --time=00-03:00:00
#SBATCH -p bigmem
#SBATCH --qos=qos-bigmem

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

cd /scratch/users/ldenies/mice_AMR/hgt/metachip
#conda activate metachip
# conda activate renv
#export PATH="/home/users/sbusi/apps/miniconda3/envs/renv/bin/R:$PATH"
#export PATH="/home/users/sbusi/apps/miniconda3/envs/renv/bin:$PATH"
#export PATH="/home/users/sbusi/apps/miniconda3/envs/anvio6/bin/mafft:$PATH"
#export mafft="/home/users/sbusi/apps/miniconda3/envs/anvio6/bin/mafft:$PATH"
#ml swenv/default-env/v1.1-20180716-production
#ml lang/Python/3.6.4-intel-2018a
#export PATH=$PATH:"~/.local/bin"
#export PATH=$PATH:"~/apps/miniconda3/bin"
#export PATH=$PATH:"/home/users/sbusi/apps/metachip/bin/"
#export PATH=$PATH:"/home/users/sbusi/apps/metachip/"
#export PATH=$PATH:"/home/users/sbusi/apps/miniconda3/envs/anvio6/bin"  # for the hmm, prodigal tools
#export PATH=$PATH:"/work/projects/ecosystem_biology/local_tools/FastTree"

conda activate METACHIP

NUM_THREADS=18

#lib=$1

MetaCHIP PI -p C100 -r g -t $NUM_THREADS -i C100 -x fa -taxon C100/C100_gtdbtk.tsv -tmp
MetaCHIP BP -p C100 -r g -t $NUM_THREADS -force -tmp

echo "== Ending run at $(date)"
