#!/bin/bash -l

for i in `cat list`
do
	mv ${i}.faa ${i}_modify.faa
        sed '/^>/ s/ .*//' ${i}_modify.faa > ${i}.faa
        rm -rf ${i}_modify.faa

	cut -f 1,9 -d ";" ${i}.gff > ${i}_select_prokka.gff
	cut -f 1,9 ${i}_select_prokka.gff > ${i}_selected.contig
	sed 's/ID=//' ${i}_selected.contig > ${i}_selected_2.contig
	sed 's/\;.*$//' ${i}_selected_2.contig > ${i}.contig
	sort -o ${i}.contig ${i}.contig
	rm -rf ${i}_select_prokka.gff ${i}_selected.contig ${i}_selected_2.contig

done



