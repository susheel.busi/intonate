#!/bin/bash -l

cd /scratch/users/ldenies/mice_AMR/hgt/metachip

#########
# Extracting Akkermansia_HGT contigs from the MetaCHIP output
#########
# making a list of the Akkermansia bins
for i in `cat samples_list`; do echo $i ;grep 'Akkermansia' "$i"/"$i"_gtdbtk.tsv | cut -f 1 > "$i"_akkermansia_bins;  done

# extracting the detected HGTs in Akkermansia bins
for sample in `cat samples_list`
do 
    for i in $(cat "$sample"_akkermansia_bins)
    do 
        file=$(find "$sample"_MetaCHIP_wd -name '*_g_detected_HGTs.txt')
#        echo $(dirname $file | awk -F "/" '{print $1}')
        grep -E -o "$i"...... $file | sed '/-->/d' | awk '!a[$0]++'
    done > "$sample"_akkermansia_contigs
done

for sample in `cat samples_list`
do 
    for i in $(cat "$sample"_akkermansia_bins | sed 's/\_.*//')
    do
      for x in $(cat "$sample"_akkermansia_contigs)
      do
        gbk=$(find "$sample"_MetaCHIP_wd -name "$i.gbk")
        grep -B 1 "$x" $gbk
      done 
    done | paste - - > "$sample"_akkermansia_locations
done

# finding a match for locus_tag and then finding another match before that for `contig_ID`
# Example: 
# awk -v valtofind="tx-145_00002" '
# /^LOCUS/{
#   id=$2
#   next
# }
# /\/locus_tag/ && $0 ~ "\""valtofind"\"$" {
#   print id,valtofind
#   id=""
# }
# '  Input_file

for sample in `cat samples_list`
do 
    for i in $(cat "$sample"_akkermansia_bins | sed 's/\_.*//')
    do
      for x in $(cat "$sample"_akkermansia_contigs)
      do
        gbk=$(find "$sample"_MetaCHIP_wd -name "$i.gbk")
        awk -v valtofind="$x" '
        /^LOCUS/{
        id=$2
        next
        }
        /\/locus_tag/ && $0 ~ "\""valtofind"\"$" {
        print id,valtofind
        id=""
        }
        ' $gbk
      done 
    done > "$sample"_akkermansia_contig_ID
done 

# merging `contig_ID` with location of the sequences
for sample in `cat samples_list`
do
  awk '{print $2"\t"$3}' "$sample"_akkermansia_locations | sed 's/\/locus_tag="//' | sed 's/"//' > "$sample"_tmp
  join -j 2 -o 2.2 2.1 1.1 "$sample"_tmp "$sample"_akkermansia_contig_ID > "$sample"_akkermansia_contig_ID_location.txt
  sed -i $'1 i\\\nbin_name\tcontig_ID\tcoordinate' "$sample"_akkermansia_contig_ID_location.txt
done

### all the "$sample"_akkermansia_contig_ID_location.txt files were sent to Laura for looking up the AMR/CRISPR locations
