#!/bin/bash -l

conda activate drep
export PATH="/home/users/ldenies/apps/ANIcalculator_v1:$PATH"

dRep dereplicate dRep_results -g all_timepoints/*fa -p 24 --debug
