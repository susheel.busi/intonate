"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2021-12-30]
Modified: []
Run: snakemake -s workflow/rules/spades_snakefile.smk --configfile config/config.yaml
Latest modification: for running metaPLASMID and metaVIRAL spades on reads
"""

configfile:"config/config.yaml"

DATA_DIR=config['data_dir']
RESULTS_DIR=config['spades_results']
IMP_DIR=config['imp_dir']
SRC_DIR = srcdir("../../scripts")
ENV_DIR = srcdir("../../envs")
SAMPLES=[line.strip() for line in open("config/all_samples.txt", 'r')]


rule all:
    input:
        expand(os.path.join(RESULTS_DIR, "metaviral/{sample}/viral_ASSEMBLY.fasta"), sample=SAMPLES),
        expand(os.path.join(RESULTS_DIR, "metaplasmid/{sample}/plasmid_ASSEMBLY.fasta"), sample=SAMPLES)

localrules: all


##########
# SPADES #
##########
rule metaplasmid:
    input:
        r1=os.path.join(IMP_DIR, "{sample}/run1/Preprocessing/mg.r1.preprocessed.fq"),
        r2=os.path.join(IMP_DIR, "{sample}/run1/Preprocessing/mg.r2.preprocessed.fq")
    output:
        os.path.join(RESULTS_DIR, "metaplasmid/{sample}/plasmid_ASSEMBLY.fasta")
    log:
        "logs/{sample}.metaplasmid.log"
    threads:
        config["metaspades"]["threads"]
    conda:
        os.path.join(ENV_DIR, "spades.yaml")
    message:
        "Running PLASMID spades on {wildcards.sample}"
    shell:
        "(date && "
        "metaspades.py --metaplasmid -k 21,33,55,77 -t {threads} -1 {input.r1} -2 {input.r2} -o $(dirname {output}) && "
        "cd $(dirname {output}) && ln -sf contigs.fasta $(basename {output}) && "
        "date) &> {log}"


rule metaviral:
    input:
        r1=os.path.join(IMP_DIR, "{sample}/run1/Preprocessing/mg.r1.preprocessed.fq"),
        r2=os.path.join(IMP_DIR, "{sample}/run1/Preprocessing/mg.r2.preprocessed.fq")
    output:
        os.path.join(RESULTS_DIR, "metaviral/{sample}/viral_ASSEMBLY.fasta")
    log:
        "logs/{sample}.metaviral.log"
    threads:
        config["metaspades"]["threads"]
    conda:
        os.path.join(ENV_DIR, "spades.yaml")
    message:
        "Running VIRUS spades on {wildcards.sample}"
    shell:
        "(date && "
        "metaspades.py --metaviral -k 21,33,55,77 -t {threads} -1 {input.r1} -2 {input.r2} -o $(dirname {output}) && "
        "cd $(dirname {output}) && ln -sf contigs.fasta $(basename {output}) && "
        "date) &> {log}"

