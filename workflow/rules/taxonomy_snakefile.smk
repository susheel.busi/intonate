"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2019-09-13]
Modified: [2021-11-09]
Run: snakemake -s workflow/rules/taxonomy_snakefile.smk --configfile config/taxonomy_config.yaml
Latest modification: for running mice_AMR taxonomy
"""

configfile:"config/taxonomy_config.yaml"

DATA_DIR=config['data_dir']
RESULTS_DIR=config['results_dir']
SRC_DIR = srcdir("../../scripts")
ENV_DIR = srcdir("../../envs")
SAMPLES=[line.strip() for line in open("config/mice_list", 'r')]
# SAMPLES=config['samples']
# DATABASE=config['database']

rule all:
    input:
        expand("{datadir}/{sample}/gtdbtk_output/gtdbtk.bac120.summary.tsv", datadir=DATA_DIR, sample=SAMPLES),
        expand("{datadir}/{sample}/gtdbtk_output/{sample}_gtdbtk.tsv", datadir=DATA_DIR, sample=SAMPLES)


localrules: all,process_taxonomy

#################
##### GTDBTK ####
#################
rule gtdbtk:
    input:
        gen="{datadir}/{sample}"
    threads:config["threads"]
    conda:
        os.path.join(ENV_DIR, "gtdbtk_new.yaml")
    output:
        bac="{datadir}/{sample}/gtdbtk_output/gtdbtk.bac120.summary.tsv"
    shell:
        """
        date &&
        export GTDBTK_DATA_PATH={config[GTDBTK][DATA]} &&
        gtdbtk classify_wf --cpus {threads} -x fa --genome_dir {input.gen} --out_dir $(dirname {output.bac}) &&
        touch gtdbtk.done && 
        date
        """

rule process_taxonomy:
    input:
        "{datadir}/{sample}/gtdbtk_output/gtdbtk.bac120.summary.tsv"
    output:
        taxonomy="{datadir}/{sample}/gtdbtk_output/{sample}_gtdbtk.tsv"
    shell:
        """
        date && 
        ln -vs {input} {output.taxonomy}
#        sed '/^user_genome/d' {input} > {output.taxonomy} &&
        date
        """
