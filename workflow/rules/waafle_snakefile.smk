"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2019-09-13]
Modified: [2021-11-12]
Run: snakemake -s workflow/rules/waafle_snakefile.smk --configfile config/config.yaml
Latest modification: for running mice_AMR taxonomy
"""

configfile:"config/config.yaml"

DATA_DIR=config['data_dir']
RESULTS_DIR=config['results_dir']
IMP_DIR=config['imp_dir']
SRC_DIR = srcdir("../../scripts")
ENV_DIR = srcdir("../../envs")
SAMPLES=[line.strip() for line in open("config/updated_mice_samples.txt", 'r')]


rule all:
    input:
#        expand("{datadir}/waafle/{sample}/{sample}.{type}", datadir=DATA_DIR, sample=SAMPLES, type=["gff", "blastout"])
        expand("{datadir}/waafle/{sample}.blastout", datadir=DATA_DIR, sample=SAMPLES)


localrules: all

##########
# WAAFLE #
##########
rule waafle:
    input:
        fna=os.path.join(IMP_DIR, "{sample}/run1/Assembly/mg.assembly.merged.fa")
    output:
#        blast="{datadir}/waafle/{sample}/{sample}.blastout",
#        gff="{datadir}/waafle/{sample}/{sample}.gff"
        blast="{datadir}/waafle/{sample}.blastout"
    threads:config["WAAFLE"]["threads"]
    conda:
        os.path.join(ENV_DIR, "waafle.yaml")
    params:
        db=config["WAAFLE"]["db"],
        taxonomy=config["WAAFLE"]["taxonomy"]
    message: 
        "Running WAAFLE for {wildcards.sample}"
    shell:  
        """
        date &&
        if [ ! -d $(dirname {output.blast}) ]; then mkdir $(dirname {output.blast}); else echo "Directory exists"; fi  &&
        cd $(dirname {output.blast}) &&
        waafle_search {input.fna} {params.db} --threads {threads} --out  $(dirname {output.blast})/{wildcards.sample}.blastout &&
        waafle_genecaller $(dirname {output.blast})/{wildcards.sample}.blastout &&
        waafle_orgscorer {input.fna} $(dirname {output.blast})/{wildcards.sample}.blastout $(dirname {output.blast})/{wildcards.sample}.gff {params.taxonomy} &&
        date
        """

# legacy code
#        mkdir $(dirname {output.blast}) &&
#        waafle_search {input.fna} {params.db} --out $(dirname {output.blast}) --threads {threads}
#        waafle_genecaller {output.blast} &&
#        waafle_orgscorer {input.fna} {output.blast} {output.gff} {params.taxonomy} --outdir --out $(dirname {output.blast}) --basename {wildcards.sample} &&
