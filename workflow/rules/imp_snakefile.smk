"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2020-02-15]
Run: snakemake -s Snakefile
"""
from pathlib import Path
# shell.executable("bash")
import pandas as pd
import os
import glob
import shutil

units_table = pd.read_table("final_mice_amr_list.txt")
SAMPLES= list(units_table.Samples.unique())

configfile:"config/imp_run_config.yaml"
DATA_DIR=config['data_dir']
RESULTS_DIR=config['results_dir']
#THREADS=config['threads']
#fastq_pair=config['fastq_pair']['bin']

rule all: 
		input: 
			expand(os.path.join(RESULTS_DIR, "{sample}/run1/status/all.done"), sample=SAMPLES)
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq"), sample=SAMPLES),
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq"), sample=SAMPLES),
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq.paired.fq"), sample=SAMPLES),
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq.paired.fq"), sample=SAMPLES),
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq.single.fq"), sample=SAMPLES), 
#			expand(os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq.single.fq"), sample=SAMPLES)
#
#rule gunzip:
#                input:
#                        r1=os.path.join(DATA_DIR, "{sample}/{sample}_1.fastq.gz"),
#                        r2=os.path.join(DATA_DIR, "{sample}/{sample}_2.fastq.gz")
#                output:	g1=os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq"), g2=os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq")
#                shell:  """
#                        date
#			gunzip -c {input.r1} > {output.g1}
#                        gunzip -c {input.r2} > {output.g2}
#			touch gunzip.done
#                        date
#                        """
#
#rule fastq_pair:
#		input:
#			f0=os.path.join(RESULTS_DIR, "{sample}"),
#			f1=os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq"),
#			f2=os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq")
#		output:	os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq.paired.fq"), os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq.paired.fq"), os.path.join(RESULTS_DIR, "{sample}/{sample}_1.fastq.single.fq"), os.path.join(RESULTS_DIR, "{sample}/{sample}_2.fastq.single.fq")
#		shell:	"""
#			date
#			cd {input.f0}
#			{config[fastq_pair][bin]} {input.f1} {input.f2}
#			touch fastq_pair.done
#			date
#			"""

rule launch_IMP:
		input: "{sample}"
		output: os.path.join(RESULTS_DIR, "{sample}/run1/status/all.done")
		shell:	"""
			date
			cd {input}
			./launchIMP.sh
		#	sbatch --time=120:00:0 -N1 -n8 -p bigmem --qos qos-bigmem  -J {input} ./{input}/{input}.runIMP.sh
		#	./{input}.runIMP.sh
			"""

