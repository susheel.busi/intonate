"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2019-11-24]
Run: snakemake -s integron_snakefile
Latest modification:
"""

import os
import glob
# import pandas 

configfile:"config/integron_config.yaml"
DATA_DIR=config['workdir']
SAMPLES=[line.strip() for line in open("bed_list", 'r')]	# if using a sample list instead of putting them in a config file
# SAMPLES=config['samples']


###########
rule all: 
		input:
			expand(os.path.join(DATA_DIR, "{sample}.bed"), sample=SAMPLES),
			expand(os.path.join(DATA_DIR, "{sample}.complete_integron.fa"), sample=SAMPLES),
			expand(os.path.join(DATA_DIR, "blast/{sample}_gc_int_blast.tsv"), sample=SAMPLES)
#			expand(os.path.join(DATA_DIR, "mash/{sample}.tab"), sample=SAMPLES)

localrules: all, bedtools


################################
# rules for bed tools analyses #
################################
rule create_bed:
		input:
			attc=os.path.join(DATA_DIR, "attC_coords.txt"),
			intI=os.path.join(DATA_DIR, "intI_coords.txt")
		output:
			os.path.join(DATA_DIR, "{sample}.bed")
		run:
			import os
			import pandas as pd

			attc=pd.read_csv(input['attc'], header=0, sep="\t")
			intI=pd.read_csv(input['intI'], header=0, sep="\t")
			
			# merging the files
			df=pd.merge(attc, intI, on='contig', how='left')

			# getting highest and lowest coordinate
			df["min"]=df[["attcstart", "attcend", "intIstart", "intIend"]].min(axis=1)
			df["max"]=df[["attcstart", "attcend", "intIstart", "intIend"]].min(axis=1)

			# removing certain columns
			df_edited=df.drop(columns=["attcstart", "attcend", "intIstart", "intIend"])

			# getting single min and max value for same contigs
			# based on here: https://jamesrledoux.com/code/group-by-aggregate-pandas and
			# here: https://www.kite.com/python/answers/how-to-apply-multiple-functions-to-multiple-columns-in-a-grouped-pandas-dataframe-in-python
			function_dict={"min": "min", "max": "max"}
			grouped = df_edited.groupby('contig', 'strand', 'score').aggregate(function_dict)

			# writing file
			df.to_csv("complete_integrons_attc_intI_coords.txt", sep='\t', index=False, header=True)
			grouped.to_csv("complete_integrons_attc_intI_MinMax_coords.txt", sep='\t', index=False, header=True)


			# Creating separate .bed files from the above
			import re

			df=pd.read_csv("complete_integrons_attc_intI_MinMax_coords.txt", skiprows=1, header=None, sep='\t')

			# making a new column and editing the text within, i.e. removing everything after "_contig"
			df['sample']=df['contig'].str.replace("_contig.*$", "", regex=True)

			# rearrange columns
			df=df[['sample', 'contig', 'min', 'max', 'strand', 'score']]
			df.to_csv("bed_coords_all.txt", sep='\t', index=False, header=None)

			# adding column names
			df.columns=["sample", "contig", "start", "end", "strand", "score"]

			# saving the contig, start and end columns to file with extension ".bed"
			for df_name, df_group in df.groupby('sample'):
			    print(df_name, df_group.head())
			    df_group.drop(['sample'], axis=1).to_csv(output[0], sep='\t', header=None, index=False )


rule bedtools:
		input:
			fna=os.path.join(DATA_DIR, "../../{sample}.fna"),
			bed=os.path.join(DATA_DIR, "{sample}.bed")
		output:	
			os.path.join(DATA_DIR, "{sample}.complete_integron.fa")
		conda:	"envs/bedtools.yaml"
		shell:	"""
			date &&\
			bedtools getfasta -s -fi {input.fna} -bed {input.bed} -fo {output}
			date
			"""
			
rule db:
		input:
			os.path.join(DATA_DIR, "gc_catalog/{sample}_attc.fna")
		output:
			touch(os.path.join(DATA_DIR, "blast/{sample}.makeblastdb.done"))
		conda:	"envs/sibeliaz.yaml"
		threads:
			config["blast"]["threads"]
		shell:	"""
			date &&\
			makeblastdb -dbtype nucl -in {input} -out $(dirname {output})/{wildcards.sample} &&\
			date
			"""

rule blast:
		input:
			int=rules.bedtools.output,
			db=os.path.join(DATA_DIR, "blast/{sample}.makeblastdb.done")
		output:
			os.path.join(DATA_DIR, "blast/{sample}_gc_int_blast.tsv")
		threads:
			config["blast"]["threads"]
		params:
			outfmt="6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"
		conda:	"envs/sibeliaz.yaml"
		shell:	"""
			date &&\
			blastn -query {input.int} -db $(dirname {output})/{wildcards.sample} -out {output} -outfmt "6 qseqid sseqid pident nident slen qstart qend length mismatch gapopen gaps evalue bitscore sseq" &&\
			date
			"""
	
#rule mash_sketch:
#		input:
#			os.path.join(DATA_DIR, "../../{sample}.fna")
#		output:
#			temp(os.path.join(DATA_DIR, "mash/{sample}.msh"))
#		threads: 1
#		conda:	"envs/mash.yaml"
#		shell:	"""
#			date &&\ 
#			ofile={output} && mash sketch -o ${{ofile%.*}} {input} &&\
#			date
#			"""
#
#rule mash_dist:
#		input:
#			gc=os.path.join(DATA_DIR, "gc_catalog/{sample}_attc.fna"), 
#			sketch=os.path.join(DATA_DIR, "mash/{sample}.msh")
#		output:
#			os.path.join(DATA_DIR, "mash/{sample}.tab")
#		threads:
#			config["mash"]["threads"]
#		conda:	"envs/mash.yaml"
#		shell:	"""
#			date &&\
#			mash dist -t -p {threads} {input.sketch} {input.gc} > {output} &&\
#			date
#			"""
		
			
