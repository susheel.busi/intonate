"""
Author: Susheel Bhanu BUSI
Affiliation: ESB group LCSB UniLU
Date: [2019-09-13]
Modified: [2021-11-09]
Run: snakemake -s workflow/rules/metachip_snakefile.smk --configfile config/config.yaml
Latest modification: for running mice_AMR taxonomy
"""

#############
import fnmatch
import pandas as pd

############
configfile:"config/config.yaml"

DATA_DIR=config['data_dir']
RESULTS_DIR=config['results_dir']
SRC_DIR = srcdir("../../scripts")
ENV_DIR = srcdir("../../envs")
SAMPLES=[line.strip() for line in open("config/mice_list", 'r')]
# SAMPLES=config['samples']
# DATABASE=config['database']

rule all:
    input:
        expand("{datadir}/{sample}/gtdbtk_output", datadir=DATA_DIR, sample=SAMPLES),
        expand("{datadir}/metachip_collated_output/{sample}_classification_coords.txt", datadir=DATA_DIR, sample=SAMPLES),
        expand("{datadir}/metachip_collated_output/{sample}_phageAMR.txt", datadir=DATA_DIR, sample=SAMPLES),
        expand("{datadir}/metachip_collated_output/{sample}_AMR_HGT.txt", datadir=DATA_DIR, sample=SAMPLES)


localrules: all, collate_contigs, concat_gbk, get_HGT_contigs, collate_taxono my_coordinates, merge_phageAMR_csv

############
# MetaCHIP #
############
rule metachip:
    input:
        gen="{datadir}/{sample}",
        taxonomy="{datadir}/{sample}/gtdbtk_output/{sample}_gtdbtk.tsv"
    output:
        metachip="{datadir}/{sample}_MetaCHIP_wd/{sample}_g_blastn_commands.txt"
    threads:config["METACHIP"]["threads"]
    conda:
#        os.path.join(ENV_DIR, "metachip.yaml")
        os.path.join(ENV_DIR, "metachip_test.yaml")
    shell:  
        """
        date &&
#        export PATH=$PATH:{config[METACHIP][path]}:{config[METACHIP][anvio_path]} && 
        export PATH=$PATH:{config[METACHIP][anvio_path]} &&
        MetaCHIP PI -p {wildcards.sample} -r g -t {threads} -i {input.gen} -x fa -taxon {input.taxonomy} -o $(dirname {output.metachip}) -force &&
        MetaCHIP BP -p {wildcards.sample} -r g -t {threads} -o $(dirname {output.metachip}) -force &&
        date
        """

rule detected_HGT:
    input:
        "{datadir}/{sample}_MetaCHIP_wd/{sample}_g_blastn_commands.txt"
    output:
        detected=temp("{datadir}/{sample}_MetaCHIP_wd/temp_{sample}_detected_HGTs.txt"),
        hgt="{datadir}/{sample}_MetaCHIP_wd/{sample}_detected_HGTs.txt"
    shell:
        """
        date &&
        file=$(find $(dirname {input}) -name '*_g_detected_HGTs.txt') && 
        cat $file | sed '/^Gene/d' > {output.detected} &&
        awk '{{ for (i=1; i<=2; i++) print $i }}' {output.detected} | sort | uniq > {output.hgt} &&
        date
        """

rule concat_gbk:
    input:
        rules.detected_HGT.output.hgt
    output:
        "{datadir}/{sample}_MetaCHIP_wd/{sample}_all.gbk"
    shell:
        """
        date &&
        find $(dirname {input}) -name '*.gbk' ! -name {output} -exec cat {{}} \; > {output} &&
        date
        """

rule get_HGT_contigs:
    input:
        hgt="{datadir}/{sample}_MetaCHIP_wd/{sample}_detected_HGTs.txt",
        gbk="{datadir}/{sample}_MetaCHIP_wd/{sample}_all.gbk"
    output:
        "{datadir}/{sample}_MetaCHIP_wd/{sample}_HGT_contig_ID_location.txt"
    message: 
        "Extracting the original contig IDs based on the detected HGT locus_tags"
    run:
        from Bio import SeqIO
        
        # Function to retrieve the contig ID from the locus_tag from the Genbank file
        def GenBankParse(gbk_file, hgt_tags):
            out_table = pd.DataFrame()

            # open file, split by record, remove last empty gbk rec
            gbk_file_list = open(gbk_file, 'r').read().split('//')[:-1]

            # iterate over recs
            for gbk_rec in gbk_file_list:

                # store contig_name and extract cds list
                contig_name = gbk_rec.split('LOCUS')[1].split()[0]
                contig_cds = gbk_rec.split('Location/Qualifiers')[1].split('\nORIGIN')[0].split('CDS  ')[1:]

                # iterate over cds list
                for cds in contig_cds:

                    # store location
                    location = cds.split()[0].replace('\n','')

                    # extract locus tag, check if in the target list
                    loc_tag = cds.split()[1].split('"')[1]
                    if loc_tag in hgt_tags:

                        # if so, remove from the target list, and append the info to the out df
                        hgt_tags.remove(loc_tag)
                        out_table = out_table.append({'locus_tag':loc_tag,'name':contig_name,'gene_coordinates':location},ignore_index=True)
            return(out_table)

        # Getting the contigs for the detected HGTs
        gbk=input.gbk
        hgt=open(input.hgt).read().split('\n')[0:-1]	# or load all your genes of interest from a file

        contigs=GenBankParse(gbk, hgt)
        contigs.to_csv(output[0], sep='\t', index=False, header=True)

####################################
# rules to collate metachip output #
####################################
rule collate_contigs:
    input:
        hgt_contigs=rules.get_HGT_contigs.output[0],
        pa="{datadir}/{sample}_MetaCHIP_wd/{sample}_detected_HGTs.txt"
    output:
        "{datadir}/metachip_collated_output/{sample}_merged_contigs_coordinates.txt"
    run:
        def find(pattern, path):
            result = []
            for root, dirs, files in os.walk(path):
                for name in files:
                    if fnmatch.fnmatch(name, pattern):
                        result.append(os.path.join(root, name))
            return result

        list=find('%s_g_detected_HGTs.txt' % wildcards.sample, os.path.dirname(input.pa))
        print(list)
        for x in list:
            assert len(list) == 1, "Found {} files instead on of only one: {}".format(len(list), list)
            hgt=pd.read_csv(list[0], sep="\t", header=0)

        # reading the contig_locations file
        contig=pd.read_csv(input.hgt_contigs, sep="\s+", header=0)

        # rename the header for the 'contig' df to match the first column of hgt
        contig.rename({"locus_tag": "Gene_1"},axis='columns',inplace =True)

        # merge the two dataframes based on common column == "Gene_1"
        merged_df = hgt.merge(contig, how = 'left', on = ['Gene_1'])

        # rename the header to match the 2nd colum of hgt
        contig.rename({"Gene_1": "Gene_2"},axis='columns',inplace =True)

        # merge the two dataframes based on common column == "Gene_1"
        final = merged_df.merge(contig, how = 'left', on = ['Gene_2'])

        # renaming some headers
#        final.rename({"contig_ID_x": "Gene1_contig", "contig_ID_y": "Gene2_contig", "coordinate_x": "Gene1_coordinate", "coordinate_y": "Gene2_coordinate",},axis='columns',inplace =True)
        final.rename({"name_x": "Gene1_contig", "name_y": "Gene2_contig", "gene_coordinates_x": "Gene1_coordinate", "gene_coordinates_y": "Gene2_coordinate",},axis='columns',inplace =True)

        # rearrange columns
        column_names=['Gene_1', 'Gene_2', 'Identity', 'end_match', 'full_length_match', 'direction', 'Gene1_contig', 'Gene2_contig', 'Gene1_coordinate', 'Gene2_coordinate']
        arranged_final=final.reindex(columns=column_names)

        # write to file
        arranged_final.to_csv(output[0], sep="\t", index=False)

rule collate_taxonomy_coordinates:
    input:
        i1=rules.collate_contigs.output[0],
        i2="{datadir}/{sample}/gtdbtk_output/{sample}_gtdbtk.tsv"
    output: 
        "{datadir}/metachip_collated_output/{sample}_classification_coords.txt"
    run:
        # reading the contig_locations file
        contig=pd.read_csv(input.i1, sep="\t", header=0)

        # new data frame with split value columns
        new = contig["direction"].str.split("-->", n = 1, expand = True)

        # making separate columns from new data frame
        contig["donor"]= new[0]
        contig["recipient"]= new[1]

        # reading the bin names from the gtdbtk output
        bin_fields=['user_genome']
        bins=pd.read_csv(input.i2, sep="\t", header=0, usecols=bin_fields)
        bins["donor"] = bins["user_genome"]
        bins["recipient"] = bins["user_genome"]
        bins.rename(columns={ bins.columns[0]: "bin" }, inplace=True)

        # merge the two dataframes based on common column == "donor"
        merged_df = contig.merge(bins, how = 'left', on = ['donor'])

        # rename the header for the 'contig' df to match the first column of hgt
        merged_df.rename({"recipient_x": "recipient"},axis='columns',inplace =True)

        # merge the two dataframes based on common column == "Gene_1"
        final = merged_df.merge(bins, how = 'left', on = ['recipient'])

        # renaming donor column and dropping unnecessary columns
        final.rename({"donor_x": "donor", "bin_x": "bin_donor", "bin_y": "bin_recipient"},axis='columns',inplace =True)
        final.drop(["recipient_y", "donor_y"], axis = 1, inplace =True)

        # importing taxonomy file, but with only the 'user_genome' and 'classificatin' columns
        fields = ['user_genome', 'classification']
        tax = pd.read_csv(input.i2, sep="\t", header=0, usecols=fields)

        # creating column for matching later and renaming the 'user_genome' column for easier merging
        tax['recipient'] = tax['user_genome']
        tax.rename({"user_genome": "donor"},axis='columns',inplace =True)

        # merging the taxonomy with the "final" dataframe, and renaming the 'classification' column
        merged = final.merge(tax[["donor", "classification"]], how = 'left', on = ['donor'])
        merged.rename({"classification": "donor_classification"},axis='columns',inplace =True)

        # getting the recipient classification by merging
        classification = merged.merge(tax[["recipient", "classification"]], how = 'left', on = ['recipient'])
        classification.rename({"classification": "recipient_classification"},axis='columns',inplace =True)

        # rearranging columns
        column_names=['Gene_1','Gene_2','Identity','end_match','full_length_match','direction','Gene1_contig','Gene2_contig','Gene1_coordinate','Gene2_coordinate','donor','recipient','donor_classification','recipient_classification','bin_donor','bin_recipient']
        arranged_classification=classification.reindex(columns=column_names)

        # write to file
        arranged_classification.to_csv(output[0], sep="\t", index=False)

rule merge_phageAMR_csv:
    input:
        m1=rules.collate_taxonomy_coordinates.output[0],
        m2="{datadir}/metachip_collated_output/Phage_Bin.csv"     # file obtained from LdN after some magic in R
    output: 
        "{datadir}/metachip_collated_output/{sample}_phageAMR.txt"
    message:
        "Merging the AMR and phage contigs from bins with the HGT info for: {wildcards.sample}"
    run:
        # reading the contig_locations file
        contig=pd.read_csv(input.m1, sep="\t", header=0)

        # reading the contig_locations file
        amr=pd.read_csv(input.m2, header=0)
        
        # merge the 'Sample' names with the 'Bin' names
        amr["names"]=amr["Sample"] + "_" + amr["Bin"]

        # create copy of bins column
        amr.rename({"names": "bin_donor"},axis='columns',inplace =True)
        amr["bin_recipient"] = amr["bin_donor"]

        # check
        contig.head()
        amr.head()

        # merge the two dataframes based on common column == "bin_donor"
        amr_tmp = amr.merge(contig, how = 'left', on = ['bin_donor'])

        # dropping the extra column
        amr_tmp.drop('recipient', axis=1, inplace=True)

        # merge the two dataframes based on common column == "bin_recipient"
        amr_tmp.rename({"bin_recipient_x": "recipient"},axis='columns',inplace =True)
        amr_merged = amr_tmp.merge(contig, how = 'left', on = ['recipient'])

        # drop rows if "Gene_1" is empty
        # amr_merged["Gene_1_x"].replace('', np.nan, inplace=True)
        amr_merged.dropna(subset=["Gene_1_x"], inplace=True)

        # drop columns with "_y" in the name
        df = amr_merged[amr_merged.columns.drop(list(amr_merged.filter(regex='_y')))]

        # Remove the "suffix" from the column names
        df.columns = df.columns.str.replace(r'_x$', '')     # OR  #df.rename(columns = lambda x: x.replace('_x$', ''))

        # dropping certain columns
        df.drop(["donor", "recipient"], axis = 1, inplace =True)

        # write to file
        df.to_csv(output[0], sep="\t", index=False)

use rule merge_phageAMR_csv as merge_AMR_bins with:
    input:
        m1=rules.collate_taxonomy_coordinates.output[0],
        m2="{datadir}/metachip_collated_output/AMR_bins.csv"     # file obtained from LdN after some magic in R; the list of all bins and the associated AMR
    output:
        "{datadir}/metachip_collated_output/{sample}_AMR_HGT.txt"
    message:
        "Merging the AMR with HGT to identify events with both in {wildcards.sample}"

