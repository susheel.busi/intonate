# amr_mice_cosmic

## Here in lie the scripts and methods used to generate data for the two following projects
    -   mice-AMR study

## A brief description of the analyses is below:
    - Snakefiles: snakemake-based scripts for launching IMP3, MAG taxonomy and quality analyses and integron coordinate retrieval 
    - config: folder containing all config files required for Snakefiles
    - envs: folder containing all conda environments, including for Rscript analyses
    - Integron_Prediction: folder including snakefile, config and supplementary scripts for integron prediction
    - scripts: folder containing supplementary scripts, including Rscripts for generating figures for the paper
    
## Step-by-step analyses 
- Detailed and additional information including explicitly described processing steps can be found in the [Wiki](https://git-r3lab.uni.lu/susheel.busi/intonate/-/wikis/mice_amr_analyses)
